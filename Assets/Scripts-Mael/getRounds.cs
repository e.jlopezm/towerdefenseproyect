using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class getRounds : MonoBehaviour
{
    [SerializeField]
    private RoundsSO RoundSO;

    // Start is called before the first frame update
    void Start()
    {
        int DeathRound = RoundSO.rounds;
        this.GetComponent<TextMeshProUGUI>().text += DeathRound;
    }
}
