using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuStart : MonoBehaviour
{
    public void StartMenu()
    {
        SceneManager.LoadScene("Inicio");
    }

}
