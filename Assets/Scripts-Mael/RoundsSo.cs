using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RoundsSO : ScriptableObject
{
    public int rounds;
    public int enemigosPorRonda;
    public int enemigosEliminados;
    public int vidaJuego;
    public float spawnTime;
}
