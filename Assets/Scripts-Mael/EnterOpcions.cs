using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class EnterOpcions : MonoBehaviour
{
    public GameObject BotoOpcions;
    public GameObject BotoJugar;
    public GameObject BotoTornarInici;
    public GameObject AudioController;


    public void EnterOptions()
    {
        BotoJugar.SetActive(false);
        BotoOpcions.SetActive(false);
        BotoTornarInici.SetActive(true);
        AudioController.SetActive(true);
    }

    public void ExitOptions()
    {
        BotoJugar.SetActive(true);
        BotoOpcions.SetActive(true);
        BotoTornarInici.SetActive(false);
        AudioController.SetActive(false);
    }

}
