using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class GetCost : MonoBehaviour
{
    public int towercost;
    private Tower tower;


    private void Start()
    {
        tower = BuilderManager.main.GetSelectedTowerAlt(towercost);
        this.GetComponent<TextMeshProUGUI>().text = "Buy: " + tower.cost + "�";
    }
}
