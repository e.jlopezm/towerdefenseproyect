using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Menu : MonoBehaviour
{
    [Header("References")]

    [SerializeField] TextMeshProUGUI coinsUI;
    [SerializeField] TextMeshProUGUI coinsUpgradeUI;
    [SerializeField] Animator anim;

    private bool isMenuOpen = true;

    public void ToogleMenu()
    {
        isMenuOpen = !isMenuOpen;
        anim.SetBool("MenuOpen", isMenuOpen);
    }

    private void OnGUI()
    {
        coinsUI.text = LevelManager.main.coins.ToString() + "�";
        coinsUpgradeUI.text = LevelManager.main.coinsUpgrade.ToString() + "$";
    }

    public void SetSelected()
    {
         
    }

    public GameObject TorretaAzul;
    public GameObject TorretaRoja;
    public GameObject TorretaVerde;
    public GameObject UpgradeAzul;
    public GameObject UpgradeRoja;
    public GameObject UpgradeVerde;
    public GameObject UpgradeToogle;
    public GameObject TowerToggle;


    public void EnterUpgrade()
    {

        UpgradeAzul.SetActive(true);
        UpgradeRoja.SetActive(true);
        UpgradeVerde.SetActive(true);
        TowerToggle.SetActive(true);
        TorretaAzul.SetActive(false);
        TorretaRoja.SetActive(false);
        TorretaVerde.SetActive(false);
        UpgradeToogle.SetActive(false);
    }

    public void ExitUpgrade()
    {
        UpgradeAzul.SetActive(false);
        UpgradeRoja.SetActive(false);
        UpgradeVerde.SetActive(false);
        TowerToggle.SetActive(false);
        TorretaAzul.SetActive(true);
        TorretaRoja.SetActive(true);
        TorretaVerde.SetActive(true);
        UpgradeToogle.SetActive(true);
    }

}

