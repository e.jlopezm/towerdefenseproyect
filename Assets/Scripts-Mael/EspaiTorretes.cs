using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspaiTorretes : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Color hoverColor;
    public GameObject particle;

    private GameObject tower;
    private Color startColor;

    private void Start()
    {
        startColor = sr.color;
    }

    private void OnMouseEnter()
    {
        sr.color = hoverColor;
    }

    private void OnMouseExit()
    {
        sr.color = startColor;
    }

    private void OnMouseDown()
    {
        if (tower != null) return;

        Tower towerToBuild = BuilderManager.main.GetSelectedTower();

        if (towerToBuild.cost > LevelManager.main.coins)
        {
            Debug.Log("Te faltan dineros para la torreta");
            return;

        }

        LevelManager.main.SpendCoins(towerToBuild.cost);



        tower = Instantiate(towerToBuild.prefab, transform.position, Quaternion.identity);
        StartCoroutine(particulaeffect());
    }

    IEnumerator particulaeffect()
    {
        particle.SetActive(true);
        yield return new WaitForSeconds(4);
        particle.SetActive(false);
    }
}
