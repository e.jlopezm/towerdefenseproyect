using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour
{
    [SerializeField] Slider AudioManager;

    public void ChangeVolume()
    {
        AudioListener.volume = AudioManager.value;
    }
}
