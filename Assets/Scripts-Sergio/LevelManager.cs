using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public static LevelManager main; //Definimos un objeto de esta clase para ir guardando el progreso del juego
    public Transform[] Points; //Waypoints
    public int coins = 0;
    public int coinsUpgrade = 0;
    public Camera Cameracamera;
    public GameObject roundsText;

    [SerializeField] private RoundsSO RoundSO;
    [SerializeField] private EnemySpawner spawner;
    [SerializeField] private TurretsSO blue;
    [SerializeField] private TurretsSO red;
    [SerializeField] private TurretsSO green;



    void Awake()
    {
        RoundSO.rounds= 0;
        RoundSO.enemigosPorRonda = 0;
        RoundSO.enemigosEliminados= 0;
        RoundSO.vidaJuego = 20;
        RoundSO.spawnTime = 2f;
        blue.vel = 20;
        red.vel = 20;
        green.vel = 20;
        blue.cooldown = 1;
        green.cooldown = 1;
        red.cooldown = 1;  
        blue.damage = 1;
        green.damage = 1;
        red.damage = 5;

        Cameracamera.GetComponent<Camera>().orthographicSize = 10;
        Cameracamera.transform.position = new Vector3(9.1f, -23.83f, -10f);
        main = this; 
    }

    private void Start()
    {
        coins = 100;
        coinsUpgrade = 50;
    }

    public void roundUp()
    {
        RoundSO.rounds++;
        coinsUpgrade += 50;
        spawner.IniciarRonda();
        RoundSO.spawnTime *= 0.75f;
        roundsText.GetComponent<RoundsText>().newRound();
        if(RoundSO.rounds == 1)
        {
            Cameracamera.transform.position = new Vector3(24.4f, -32.3f, -10f);
            Cameracamera.GetComponent<Camera>().orthographicSize = 20;
        }
        else if ( RoundSO.rounds == 2) {
            Cameracamera.transform.position = new Vector3(24.4f, -32.3f, -10f);
            Cameracamera.GetComponent<Camera>().orthographicSize = 30;
        }
        else if (RoundSO.rounds == 3)
        {
            Cameracamera.transform.position = new Vector3(10.1f, -11f, -10f);
            Cameracamera.GetComponent<Camera>().orthographicSize = 40;
        }
    }

    public void IncreaseCoins(int amount)
    {
        coins += amount;
    }

    public bool SpendCoins(int amount)
    {
        if(coins >= amount) {
            coins-=amount;
            return true;
        }
        else
        {
            Debug.Log("No pots comprar aquesta torre, et falten diners");
            return false;
        }
    }

}
