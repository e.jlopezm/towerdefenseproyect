using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private GameObject[] enemyPrefabs;
    [SerializeField] private Transform[] WaveInitialPoints;
    [SerializeField] private RoundsSO round;


    [Header("Attributes")]
    //[SerializeField] private float spawnTime = 2f; // Time between enemy spawns
    [SerializeField] private float waveWaitTime = 5f; // Time between waves (replace "waitTime" with "waveWaitTime" for clarity)

    private int enemiesLeftToSpawn;

    private void Start()
    {
        round.spawnTime = 2f;
        IniciarRonda();
    }

    private IEnumerator SpawnEnemyCoroutine()
    {
        
        enemiesLeftToSpawn = enemiesPerWave();
        round.enemigosPorRonda = enemiesPerWave();
        // Spawn enemies until the wave is complete
        while (enemiesLeftToSpawn > 0)
        {
            yield return new WaitForSeconds(round.spawnTime);

            SpawnEnemy();
            enemiesLeftToSpawn--;
            
        }

        yield return new WaitForSeconds(waveWaitTime);

    }

    public void IniciarRonda()
    {
        StartCoroutine(SpawnEnemyCoroutine());
    }

    private void SpawnEnemy()
    {
        GameObject enemy = enemyPrefabs[Random.Range(0,enemyPrefabs.Length)]; // Considr using a random index for enemy variety
        int index = round.rounds;
        if (index > 3)
        {
            index = 3;
        }
        Instantiate(enemy, WaveInitialPoints[index].position, enemy.transform.rotation);
    }


    private int enemiesPerWave() 
    {
        switch (round.rounds)
        {
            case 0:
                return 4;
            case 1:
                return 15;
            case 2:
                return 24;
            case 3:
                return 40;
            default:
                return round.rounds*20;
        }
    }
}
