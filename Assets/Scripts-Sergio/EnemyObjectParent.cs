using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyObjectParent : ScriptableObject
{
    public int vida;
    public float speed;
    public int givencoins;
    public Boolean fly;
}
