using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class EnemyScript : MonoBehaviour
{

    [Header("References")]
    [SerializeField] protected Rigidbody2D rb;
    [SerializeField] protected EnemyObjectParent enemy;
    [SerializeField] protected Animator anim;
    [SerializeField] protected RoundsSO round;
   
    public int pointIndex;
    private Boolean dreta = true;
    public int lifePoints;
    public float speedOverRound;
 
    void Start()
    {
        if (round.rounds < 3)
        {
            this.pointIndex = GetPointIndexForWave(round.rounds);

        }
        else
        {
            this.pointIndex = GetPointIndexForWave(3);
        }
        CanviDireccio();
        this.lifePoints = enemy.vida;
    }

    protected void CanviDireccio()
    {
        if (pointIndex >= LevelManager.main.Points.Length)
        {
            round.enemigosEliminados++;
            Destroy(rb.gameObject);
            round.vidaJuego--;
            if (round.vidaJuego <= 0)
            {
                SceneManager.LoadScene("GameOver");
            }
            if (round.enemigosEliminados == round.enemigosPorRonda)
            {
                round.enemigosEliminados = 0;
                LevelManager.main.roundUp();
            }
            return;
        }

        if (LevelManager.main.Points[pointIndex].gameObject.tag == "mirarDreta" && !dreta)
        {
            Rotate();
        }
        else if (LevelManager.main.Points[pointIndex].gameObject.tag == "mirarEsquerra" && dreta)
        {
            Rotate();
        }

        this.GetComponent<Rigidbody2D>().velocity = (LevelManager.main.Points[pointIndex].position-this.transform.position).normalized* enemy.speed*(round.rounds+1);
    }

    private void Rotate()
    {
        dreta = !dreta;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }


    protected int GetPointIndexForWave(int wave)
    {
        switch (wave)
        {
            case 0:
                return 15;
            case 1:
                return 10;
            case 2:
                return 8;
            case 3:
                return 0;
            default:
                return -1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "enemy" && collision.tag != "tower_base" && collision.tag != "tower" && collision.gameObject.tag != "bullet") {
            pointIndex++;
            CanviDireccio();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            this.lifePoints -= collision.gameObject.GetComponent<Bullet>().damage;

            Destroy(collision.gameObject);

            if (this.lifePoints<= 0)
            {
                LevelManager.main.IncreaseCoins(enemy.givencoins);
                Destroy(this.gameObject);
                round.enemigosEliminados++;
                if(round.enemigosEliminados == round.enemigosPorRonda)
                {
                    round.enemigosEliminados = 0;
                    LevelManager.main.roundUp();
                }
            }

            
        }
    }
}
