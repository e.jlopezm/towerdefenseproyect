using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

public class Witch : EnemyScript
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "enemy" && collision.tag != "tower_base" && collision.tag != "tower" && collision.gameObject.tag != "bullet")
        {
            pointIndex++;
            CanviDireccio();
            if(pointIndex < LevelManager.main.Points.Length)
                CanviarAnimacio();
        }
    }

    private void CanviarAnimacio()
    {
        //Debug.Log(LevelManager.main.Points[pointIndex].gameObject.tag);

        if (LevelManager.main.Points[pointIndex].gameObject.tag == "mirarAdalt")
        {
            this.anim.SetBool("adalt", true);
            this.anim.SetBool("abaix", false);
            this.anim.SetBool("costat", false);
        }
        else if (LevelManager.main.Points[pointIndex].gameObject.tag == "mirarAbaix")
        {
            this.anim.SetBool("adalt", false);
            this.anim.SetBool("abaix", true);
            this.anim.SetBool("costat", false);
        }
        else
        {
            this.anim.SetBool("adalt", false);
            this.anim.SetBool("abaix", false);
            this.anim.SetBool("costat", true);
        }
    }

    private void Start()
    {
        this.pointIndex = GetPointIndexForWave(round.rounds);
        CanviarAnimacio();
        CanviDireccio();
        this.lifePoints = enemy.vida;
    }
}
