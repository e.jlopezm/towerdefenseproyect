using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;
using static UnityEngine.GraphicsBuffer;

public class Bullet : MonoBehaviour
{
    public int damage;
    public GameObject enemy;
    public TurretsSO projectil;

    private void Start()
    {
        this.damage = projectil.damage;
    }

    void Update()
    {
        if(enemy.gameObject == null)
        {
            Destroy(this.gameObject);
            return;
        }
        Vector2 distancia = enemy.transform.position - this.transform.position;
        this.transform.up = distancia;
        distancia.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = distancia * projectil.vel;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            Destroy(this.gameObject);
        }
    }
}
