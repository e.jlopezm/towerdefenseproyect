using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;
using UnityEditor.Animations;

public abstract class Turret : MonoBehaviour
{
    public GameObject enemy;
    public TurretsSO turret;
    [SerializeField]
    private ShootSO projectils;
    public GameObject bala;
    protected bool shooting;
    [SerializeField] protected Animator turretAnim;
    [SerializeField] private AnimatorController controller;
    [SerializeField] protected Sprite[] spritesUpgrade;
    private int upgrade;
    private bool entry;
    public delegate void delegate_shoot();
    public delegate_shoot shootD;

    void Start()
    {
        upgrade = turret.upgrade;
        this.GetComponent<SpriteRenderer>().sprite = spritesUpgrade[0];

    }

    // Update is called once per frame
    void Update()
    {
        if (entry)
        {
            Apuntar();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy" )
        {
            entry = true;
            shooting = true;
            enemy = collision.gameObject;
            turretAnim.SetBool("Shoot", true);
            turretAnim.speed = 1;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            turretAnim.speed = 1;
            shooting = false;
            entry = false;
            turretAnim.SetBool("Shoot", false);
        }
    }

    public virtual void Apuntar()
    {
        if(enemy.gameObject != null) { 
            Vector2 distance = enemy.transform.position - this.transform.position;
            this.transform.up = distance;
            distance.Normalize();
        }
    }

    public void shoot()
    {
        
        if (enemy.gameObject != null)
        {
            ShootSO projectilActual = projectils;
            GameObject newBala = Instantiate(bala);
            newBala.GetComponent<Bullet>().enemy = enemy;
            newBala.transform.position = new Vector2(this.transform.position.x - 0.01f, this.transform.position.y);
            newBala.GetComponent<Rigidbody2D>().velocity = new Vector2(projectilActual.vel, projectilActual.vel);
            newBala.GetComponent<SpriteRenderer>().color = projectilActual.color;
            newBala.transform.localScale *= projectilActual.scale;
            newBala.GetComponent<Bullet>().damage = projectilActual.damage;
        }
        else turretAnim.SetBool("Shoot", false);

    }

    public void upgradeFunction()
    {
        
        if (this.turret.upgradeCost <= LevelManager.main.coinsUpgrade)
        {
            LevelManager.main.coinsUpgrade -= this.turret.upgradeCost;
            if (this.turret.upgrade < 2)
            {
                this.turret.upgrade++;
                Debug.Log("upgrade" + this.turret.upgrade);

               /* if (this.turret.upgrade == 1 || this.turret.upgrade == 2)
                {
                    //this.turretAnim.gameObject.SetActive(true);
                    //this.turretAnim.GetComponent<AnimatorController>() = controller;
                    //turretAnim.SetBool("U1", true);
                    //this.GetComponent<SpriteRenderer>().sprite = spritesUpgrade[this.turret.upgrade];
                }*/
            }
            turret.damage++;
            turret.cooldown++;
            turret.vel += 5;
            turretAnim.speed *= turret.cooldown;
        }
        else
        {
            Debug.Log("Te falta dinero para el upgrade");
        }
    }
}
