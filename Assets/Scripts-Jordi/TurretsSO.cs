using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TurretsSO : ScriptableObject
{
    // Las torres tendr�n da�o, cooldown, los niveles
    // de upgrade y si atacan a objetivos voladores o no
    public float vel;
    public float scale;
    public Color color;
    public int damage;
    public float cooldown;
    public int upgrade;
    public int upgradeCost;
    public bool flying_atack;
}
