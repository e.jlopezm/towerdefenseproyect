using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoundsText : MonoBehaviour
{
    [SerializeField] private RoundsSO round;
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Rounds: " + round.rounds;
    }

    public void newRound()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Rounds: " + round.rounds;
    }

}
