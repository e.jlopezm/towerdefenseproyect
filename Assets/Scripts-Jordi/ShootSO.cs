using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ShootSO : ScriptableObject
{
    public float vel;
    public float cooldown;
    public float scale;
    public Color color;
    public int damage;
}
